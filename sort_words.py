#!/usr/bin/env python3

import csv
from argparse import ArgumentParser, FileType

parser = ArgumentParser()
parser.add_argument("--delimiter", "-d", default=",")
parser.add_argument("--input", "-i", default="-", type=FileType("r"))
parser.add_argument("--output", "-o", default="-", type=FileType("w"))
args = parser.parse_args()

result = []
reader = csv.reader(args.input, delimiter=args.delimiter)
for row in reader:
    result.append(sorted(row))

writer = csv.writer(args.output)
writer.writerows(sorted(result))

args.output.close()
