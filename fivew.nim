
import std/strformat
import std/bitops
import std/tables
import std/sequtils
import std/algorithm
import std/strutils

template to_ord(c: char, offset: int = 0): untyped = ord(c) - ord('a') + offset

proc to_bitset(s: string): uint32 =
    result = 0u32
    for c in s:
        result.setBit(to_ord(c))

template filter_by_length(id: untyped, it: untyped, length: int) =
    var id = newSeq[string]()
    for word in it:
        if len(word) == length:
            id.add(word)

iterator prune_keys_plus(possible_keys: openArray[uint32], bitset: uint32): uint32 =
    for key in possible_keys:
        if key > bitset and bitand(key, bitset) == 0:
            yield key

filter_by_length(words, lines("words_alpha.txt"), 5)

when defined(DEBUG):
  echo "5 letter words ", words.len

var reverse_index = initTable[uint32, seq[string]]()

for word in words:
    let num = to_bitset(word)
    if countSetBits(num) != word.len():
        continue
    if not reverse_index.hasKey(num):
        reverse_index[num] = @[]
    reverse_index[num].add(word)

when defined(DEBUG):
  echo "without permutations ", reverse_index.len

proc compute_result(word_seq: seq[uint32], words: seq[string], word_results: var seq[seq[string]]) =
    if word_seq.len() == 0:
        word_results.add(words)
    else:
        let first = word_seq[0]
        let tail = word_seq[1..^1]
        for word in reverse_index[first]:
            compute_result(tail, concat(words, @[word]), word_results)

proc find_words(seq_len: uint, cur_seq: seq[uint32], possible_keys: seq[uint32], word_results: var seq[seq[string]]) =
    if seq_len == 0:
        compute_result(cur_seq, @[], word_results)
    else:
        for w in possible_keys:
            let pruned_keys = toSeq(prune_keys_plus(possible_keys, w))
            find_words(seq_len - 1, concat(cur_seq, @[w]), pruned_keys, word_results)


var possible_keys = toSeq(reverse_index.keys())
possible_keys.sort()

var word_results = newSeq[seq[string]]()
find_words(5, @[], possible_keys, word_results)

when defined(DEBUG):
    echo "number of solutions ", word_results.len()

when defined(PRINT_ALL_RESULTS):
    for result in word_results:
        echo result.join(",")

